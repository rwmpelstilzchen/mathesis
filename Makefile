all:
	latexmk -xelatex -file-line-error thesis

pvc:
	latexmk -silent -pvc -file-line-error thesis

clean:
	-rm *.aux *.bbl *.blg *.log *.toc *.url *.cut *.run.xml *.bst *.bcf *.fls *.fdb_latexmk *.out *.dvi *.idx *.ilg *.ind

distclean: clean
	-rm *.pdf
