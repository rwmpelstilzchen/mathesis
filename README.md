<h1><i>se ðe ís soð wysdom · ⁊ sawla líf</i><br/>A Textual-Structural Study of the Demonstrative Pronouns in Old English:<br/>classification, characterization and description<br/>of the examples in Ælfric’s <i>Lives of Saints</i></h1>

Hi,

I’m a linguist; I reverse-engineer language. This is my MA thesis, about Old English syntax. It is written in Hebrew, as required by the Hebrew University of Jerusalem, where I’ve written it (oh, it is *so* nice to use the perfect here…).

Here is the abstract:

> Description of the syntactical structures in Old English, in which the demonstrative pronouns (<span style="font-variant: small-caps;">se</span> and <span style="font-variant: small-caps;">ðis</span>) occur independently of a following nominal phrase: structural classification, characterization and analysis of their internal structure and their function in the text by a comprehensive examination of the examples in the corpus, Ælfric’s *Lives of Saints*.

> Such an exhaustive survey of the relevant examples in a text of this length has never been done. A clearer, more complete profile of the functions of the pronouns in question emerges from it, with hitherto undescribed findings. These contribute to a subtler understanding of Ælfric’s language, on a structural linguistic basis.

Fascinating, isn’t it? Really, it’s cracking a code about thousand years old and gaining a better understanding of a wonderful language (and reading funny texts along the way)! I love science.

I uploaded a digital edition of the corpus (typed in twice by hand and `vimdiff`ed!) to [Wikisource](http://en.wikisource.org/wiki/%C3%86lfric%27s_Lives_of_Saints).

BibLaTeX code:

```bibtex
@MASTERSTHESIS {ronen.j:2015:demonstratives,
    author      = {Ronén, Júda},
	title       = {\emph{se ðe ís soð wysdom~· & sawla líf} —
                   A Textual-Structural Study of the Demonstrative Pronouns in Old English},
    subtitle    = {classification, characterization and description
                   of the examples in Ælfric's Lives of Saints},
    institution = {Department of Linguistics, the Hebrew University of Jerusalem},
	year        = {2015},
}
```

The bibliography is located [here](https://gitlab.com/rwmpelstilzchen/bibliography.bib) in a different repository.

I’m currently working on my PhD thesis, which will be about Modern Welsh text-syntax. Stay tuned! :-p

You are encouraged to send me feedback ([contact information](http://me.digitalwords.net/)).



## Why do I make the the source and history of git commits public?

For two reasons:
* I truly believe openness and transparency can benefit science (and not only science) greatly. Gratis [open access](http://en.wikipedia.org/wiki/File:PhD_Comics_Open_Access_Week_2012.ogv) is awesome, but more can be achieved through libre OA and making the process of making science open and transparent.
* It’s [fun](http://en.wikipedia.org/wiki/Just_for_Fun).

Inspiration: [wspr’s thesis](https://github.com/wspr/thesis).

What isn’t public? My database and my personal notes, both are too idiosyncratic to be published…



## Copyright

Now personally I don’t believe the whole concept of intellectual property is any good. Nevertheless, I ask you not to redistribute this material without giving appropriate credit (formal licence: [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)).
